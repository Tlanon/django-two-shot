from django.urls import path
from receipts.views import get_all_receipts, create_receipt

urlpatterns = [
    path("", get_all_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
]
